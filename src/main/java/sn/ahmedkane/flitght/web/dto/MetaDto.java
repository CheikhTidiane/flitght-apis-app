package sn.ahmedkane.flitght.web.dto;


import sn.ahmedkane.flitght.persistance.entities.Meta;

import java.io.Serializable;
import java.util.Date;

public class MetaDto implements Serializable {

    private String externalId;
    private Date createdAt;


    public MetaDto(){}

    public MetaDto(Meta meta){
        this.externalId = meta.getExternalId();
        this.createdAt = meta.getCreatedAt();
    }


    public static MetaDto parse(Meta meta){
        return new MetaDto(meta);
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
