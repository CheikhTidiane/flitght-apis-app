package sn.ahmedkane.flitght.web.dto.response;

import sn.ahmedkane.flitght.web.dto.AirlineRapidapiDto;

import java.io.Serializable;
import java.util.List;

public class ResponseAirline implements Serializable {
    private int page;
    private int size;
    private List<AirlineRapidapiDto> contents;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<AirlineRapidapiDto> getContents() {
        return contents;
    }

    public void setContents(List<AirlineRapidapiDto> contents) {
        this.contents = contents;
    }
}
