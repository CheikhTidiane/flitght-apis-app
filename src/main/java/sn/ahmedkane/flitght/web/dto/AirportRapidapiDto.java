package sn.ahmedkane.flitght.web.dto;

import sn.ahmedkane.flitght.persistance.entities.Airport;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

public class AirportRapidapiDto implements Serializable {
  private int id;
  private String iata;
  private String icao;
  private String name;
  private String shortName;
  private String municipalityName;
  private Map<String,Double> location;
    private String countryCode;
 // private String location;
  /*private String street_number;
  private String street;
  private String city;
  private String county;

  private String state;
  private String country_iso;
  private String country;
  private String postal_code;
  private String phone;
  private double latitude;
  private double longitude;
  private String country_code;
  private String city_code;
  private double uct;
  private String website;
  private String name_translations;

   */


  public AirportRapidapiDto(){}
  public AirportRapidapiDto(Airport airport){

  }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getMunicipalityName() {
        return municipalityName;
    }

    public void setMunicipalityName(String municipalityName) {
        this.municipalityName = municipalityName;
    }

    public Map<String, Double> getLocation() {
        return location;
    }

    public void setLocation(Map<String, Double> location) {
        this.location = location;
    }


}
