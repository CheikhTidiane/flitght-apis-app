package sn.ahmedkane.flitght.web.dto.response;

import sn.ahmedkane.flitght.web.dto.AirlineRapidapiDto;
import sn.ahmedkane.flitght.web.dto.AirlineTravelPayoutsDto;

import java.util.List;

public class ResponseAirlineTravelPayouts {

    private int page;
    private int size;
    private List<AirlineTravelPayoutsDto> contents;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<AirlineTravelPayoutsDto> getContents() {
        return contents;
    }

    public void setContents(List<AirlineTravelPayoutsDto> contents) {
        this.contents = contents;
    }
}
