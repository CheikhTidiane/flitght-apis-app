package sn.ahmedkane.flitght.web.dto;

import sn.ahmedkane.flitght.persistance.entities.Airline;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class AirlineTravelPayoutsDto implements Serializable {

    private String code;

    private String name;

    private Map<String,String> name_translations;

    private MetaDto meta;

    public AirlineTravelPayoutsDto(){}
    public AirlineTravelPayoutsDto(Airline airline){
        if(Objects.nonNull(airline.getIataCode())){
            this.code = airline.getIataCode();
        }

        if(Objects.nonNull(airline.getName())){
            this.name = airline.getName();
        }
        if(Objects.nonNull(airline.getLinkedMeta())){
            this.meta = MetaDto.parse(airline.getLinkedMeta());
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getName_translations() {
        return name_translations;
    }

    public void setName_translations(Map<String, String> name_translations) {
        this.name_translations = name_translations;
    }

    public MetaDto getMeta() {
        return meta;
    }

    public void setMeta(MetaDto meta) {
        this.meta = meta;
    }

    public static AirlineTravelPayoutsDto parse(Airline airline){
      return new AirlineTravelPayoutsDto(airline);
    }
    public static List<AirlineTravelPayoutsDto> parse(List<Airline> airlines){
        return airlines.stream().map(AirlineTravelPayoutsDto::parse).collect(Collectors.toList());
    }


}
