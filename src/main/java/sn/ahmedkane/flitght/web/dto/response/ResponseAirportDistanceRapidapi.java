package sn.ahmedkane.flitght.web.dto.response;


import sn.ahmedkane.flitght.web.dto.AirportRapidapiDto;
import sn.ahmedkane.flitght.web.dto.AirportsDistanceToFromDto;

import java.util.List;

public class ResponseAirportDistanceRapidapi {
    private int page;
    private int size;
    private List<AirportsDistanceToFromDto> contents;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<AirportsDistanceToFromDto> getContents() {
        return contents;
    }

    public void setContents(List<AirportsDistanceToFromDto> contents) {
        this.contents = contents;
    }
}
