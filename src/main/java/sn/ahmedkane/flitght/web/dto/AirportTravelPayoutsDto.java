package sn.ahmedkane.flitght.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.core.util.Json;
import sn.ahmedkane.flitght.persistance.entities.Airline;
import sn.ahmedkane.flitght.persistance.entities.Airport;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class AirportTravelPayoutsDto implements Serializable {
    private String code;
    private String name;
    private String iata_type;
    private boolean flightable;
    private Map<String,Double> coordinates;
    private String time_zone;
    private Map<String,String> name_translations;
    private String country_code;
    private String city_code;

    public AirportTravelPayoutsDto(){}

    public AirportTravelPayoutsDto(Airport airport){
        if(Objects.nonNull(airport.getCode())) this.code = airport.getCode();
        if(Objects.nonNull(airport.getName())) this.name = airport.getName();

        if(Objects.nonNull(airport.getIataType())) this.iata_type = airport.getIataType();
        if(Objects.nonNull(airport.getLatitude()) && Objects.nonNull(airport.getLongitude())){
            HashMap<String,Double> coordonate = new HashMap<>();
            coordonate.put("lat",airport.getLatitude());
            coordonate.put("lon",airport.getLongitude());
            this.coordinates = coordonate;
        }
        if(Objects.nonNull(airport.getTime_zone())) this.time_zone = airport.getTime_zone();
        if(Objects.nonNull(airport.getCountry())) this.country_code = airport.getCountry();
        if(Objects.nonNull(airport.getCity())) this.city_code = airport.getCity();
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIata_type() {
        return iata_type;
    }

    public void setIata_type(String iata_type) {
        this.iata_type = iata_type;
    }

    public boolean isFlightable() {
        return flightable;
    }

    public void setFlightable(boolean flightable) {
        this.flightable = flightable;
    }


    public String getTime_zone() {
        return time_zone;
    }

    public void setTime_zone(String time_zone) {
        this.time_zone = time_zone;
    }


    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCity_code() {
        return city_code;
    }


    public void setCity_code(String city_code) {
        this.city_code = city_code;
    }

    public Map<String, Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Map<String, Double> coordinates) {
        this.coordinates = coordinates;
    }

    public Map<String, String> getName_translations() {
        return name_translations;
    }

    public void setName_translations(Map<String, String> name_translations) {
        this.name_translations = name_translations;
    }

    public static AirportTravelPayoutsDto parse(Airport airport){
        return new AirportTravelPayoutsDto(airport);
    }

    public static List<AirportTravelPayoutsDto> parse(List<Airport> airports){
        return airports.stream().map(AirportTravelPayoutsDto::parse).collect(Collectors.toList());
    }
}


