package sn.ahmedkane.flitght;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sn.ahmedkane.flitght.utils.IdentifierGenerator;


@Configuration
public class SessionIdentifierGeneratorConfig {

    @Bean
    public IdentifierGenerator identifierGenerator() {
        return new IdentifierGenerator();
    }
}
