package sn.ahmedkane.flitght;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class FlitghtDataAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlitghtDataAppApplication.class, args);
	}

}
