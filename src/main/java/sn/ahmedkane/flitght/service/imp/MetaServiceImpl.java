package sn.ahmedkane.flitght.service.imp;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import sn.ahmedkane.flitght.persistance.entities.Meta;
import sn.ahmedkane.flitght.persistance.repository.MetaRepository;
import sn.ahmedkane.flitght.service.IMetaService;
import sn.ahmedkane.flitght.utils.IdentifierGenerator;
import sn.ahmedkane.flitght.web.dto.MetaDto;

import javax.transaction.Transactional;
import java.util.Date;

@Service
@Transactional
public class MetaServiceImpl implements IMetaService {


    @Autowired
    private MetaRepository metaRepository;

    @Autowired
    private IdentifierGenerator identifierGenerator;



    @Override
    public Meta createNew(String resourceType) {
        Meta meta = new Meta();
        meta.setExternalId(identifierGenerator.generate(4, 8, true));
        meta.setCreatedAt(new Date());
        meta.setResourceType(resourceType);
        meta.setAuthor(2); //TODO: TEMPORARY, must be replaced by real logged in user account
        Meta savedMeta = metaRepository.save(meta);
        if (savedMeta == null) throw new RestClientException("Erreur au moment de la création du Meta");
        return savedMeta;
    }


    @Override
    public MetaDto parse(Meta meta) {
        return new MetaDto(meta);
    }

    @Override
    public Meta findByExternalId(String linkedMeta) {
        return this.metaRepository.findByExternalId(linkedMeta);
    }


    @Override
    public MetaDto save(Meta meta) {
        return new MetaDto(this.metaRepository.save(meta));
    }
}
