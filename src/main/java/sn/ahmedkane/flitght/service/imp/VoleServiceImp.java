package sn.ahmedkane.flitght.service.imp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.Response;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.ahmedkane.flitght.service.IVoleService;
import sn.ahmedkane.flitght.web.dto.SearchDto;
import sn.ahmedkane.flitght.web.dto.VolViewDto;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;

@Service
@Transactional
public class VoleServiceImp implements IVoleService {

   /* @Autowired
    private FlitghtRestController service;
*/

    private List<VolViewDto> allResult = new ArrayList<>();

    @Override
    public List<VolViewDto> getFlitghtByAirport(SearchDto searchDto) {

        try{
            AsyncHttpClient client = new DefaultAsyncHttpClient();

            CompletableFuture<Response> response =
                    client.prepare("GET", "https://travelpayouts-travelpayouts-flight-data-v1.p.rapidapi.com/data/routes.json")
                            .setHeader("x-access-token", "e91d3d4939msha8db30c5f35418ep161835jsn47dccd980fa4")
                            .setHeader("x-rapidapi-host", "travelpayouts-travelpayouts-flight-data-v1.p.rapidapi.com")
                            .setHeader("x-rapidapi-key", "e91d3d4939msha8db30c5f35418ep161835jsn47dccd980fa4")
                            .execute()
                            .toCompletableFuture();


            ObjectMapper mapper = new ObjectMapper();

            response.thenAccept(data-> {
                        try {
                            setData(mapper.readValue(data.getResponseBody(), mapper.getTypeFactory().constructCollectionType(List.class, VolViewDto.class)));
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                    }

            ).join();
            client.close();

        }catch (Exception e){
            // TODO : Envoyer l'erreur au front end
        }
        System.out.println("ok ... "+this.allResult.get(0).toString());

        return this.allResult;
    }


    public void setData(List<VolViewDto> volViewDtos){
        this.allResult = volViewDtos;
    }
}
