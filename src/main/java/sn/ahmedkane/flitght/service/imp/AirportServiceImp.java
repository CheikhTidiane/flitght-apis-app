package sn.ahmedkane.flitght.service.imp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestClientException;
import sn.ahmedkane.flitght.persistance.entities.Airline;
import sn.ahmedkane.flitght.persistance.entities.Airport;
import sn.ahmedkane.flitght.persistance.repository.AirportRepository;
import sn.ahmedkane.flitght.service.IAirportService;
import sn.ahmedkane.flitght.service.IMetaService;
import sn.ahmedkane.flitght.utils.StatusResponse;
import sn.ahmedkane.flitght.web.dto.AirlineRapidapiDto;
import sn.ahmedkane.flitght.web.dto.AirportRapidapiDto;
import sn.ahmedkane.flitght.web.dto.AirportTravelPayoutsDto;
import sn.ahmedkane.flitght.web.dto.AirportsDistanceToFromDto;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirportDistanceRapidapi;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirportRapidapi;
import sn.ahmedkane.flitght.web.dto.response.ResponseTravelPayoutsdapi;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

@Service
@Transactional
public class AirportServiceImp implements IAirportService {

    private List<AirportRapidapiDto> allResultRapidapi = new ArrayList<>();
    private List<AirportTravelPayoutsDto> allResultTravelPayouts = new ArrayList<>();
    private List<AirportsDistanceToFromDto> airportsDistanceToFromDto = null;

    @Autowired
    private AirportRepository airportRepository;

    @Autowired
    private IMetaService metaService;


    @Override
    public ResponseTravelPayoutsdapi getAllAirportFromTravelPayouts() {
        ResponseTravelPayoutsdapi result = new ResponseTravelPayoutsdapi();
        result.setPage(0);


        AsyncHttpClient client = new DefaultAsyncHttpClient();
        CompletableFuture <Response> response =  client.prepare("GET", "http://api.travelpayouts.com/data/en/airports.json")
                .setHeader("x-rapidapi-host", "airport-info.p.rapidapi.com")
                .setHeader("x-rapidapi-key", "e91d3d4939msha8db30c5f35418ep161835jsn47dccd980fa4")
                .execute()
                .toCompletableFuture();



        ObjectMapper mapper = new ObjectMapper();

                response.thenAccept(data->{
                    try {
                        setDataAirportFromTravelPayouts(mapper.readValue(data.getResponseBody(), mapper.getTypeFactory().constructCollectionType(List.class, AirportTravelPayoutsDto.class)));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                })
                .join();

        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        result.setSize(this.allResultRapidapi.size());
        result.setContents(this.allResultTravelPayouts);

        return result;
    }


    @Override
    public ResponseAirportRapidapi getAllAirportFromRapidapi() {
        ResponseAirportRapidapi result = new ResponseAirportRapidapi();
        result.setPage(0);


        AsyncHttpClient client = new DefaultAsyncHttpClient();
        CompletableFuture <Response> response =  client.prepare("GET", "http://api.travelpayouts.com/data/en/airports.json")
                .setHeader("x-rapidapi-host", "airport-info.p.rapidapi.com")
                .setHeader("x-rapidapi-key", "e91d3d4939msha8db30c5f35418ep161835jsn47dccd980fa4")
                .execute()
                .toCompletableFuture();



        ObjectMapper mapper = new ObjectMapper();

        response.thenAccept(data->{
            try {
                setDataAirportFromTravelPayouts(mapper.readValue(data.getResponseBody(), mapper.getTypeFactory().constructCollectionType(List.class, AirportTravelPayoutsDto.class)));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        })
                .join();

        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        result.setSize(this.allResultRapidapi.size());
        result.setContents(this.allResultRapidapi);

        return result;
    }

    @Override
    public ResponseTravelPayoutsdapi getAll(int page, int size) {
        ResponseTravelPayoutsdapi result = new ResponseTravelPayoutsdapi();
        if(size == 0 ) size = 25;
        result.setPage(page);
        result.setContents(AirportTravelPayoutsDto.parse(this.airportRepository.findAll(PageRequest.of(page,size)).getContent()));
        result.setSize(result.getContents().size());
        return result;
    }

    @Override
    public StatusResponse create(AirportTravelPayoutsDto dtoCreate) {
        StatusResponse statusResponse = new StatusResponse();
        Airport airport = new Airport();

        if(Objects.nonNull(dtoCreate.getCode())) airport.setCode(dtoCreate.getCode());
        if(Objects.nonNull(dtoCreate.getName())) airport.setName(dtoCreate.getName());
        if(Objects.nonNull(dtoCreate.getCity_code())) airport.setCity(dtoCreate.getCity_code());
        if(Objects.nonNull(dtoCreate.getCountry_code())) airport.setCountry(dtoCreate.getCountry_code());
        if(Objects.nonNull(dtoCreate.getCoordinates())){
           // airport.setLatitude(dtoCreate.getCoordinates().get("lat"));
           // airport.setLongitude(dtoCreate.getCoordinates().get("lon"));
        }
        if(Objects.nonNull(dtoCreate.getTime_zone())) airport.setTime_zone(dtoCreate.getTime_zone());
        if(Objects.nonNull(dtoCreate.getIata_type())) airport.setIataType(dtoCreate.getIata_type());

        airport.setLinkedMeta(this.metaService.createNew(AirportTravelPayoutsDto.class.getSimpleName()));
        Airport response = this.airportRepository.save(airport);
        if(Objects.nonNull(response)){
            statusResponse.setCode(200);
            statusResponse.setMessage("Enrégistrement de l'aéroport ( "+response.getName()+" ) réussit");
        }else{
            statusResponse.setCode(403);
            statusResponse.setMessage("Enrégistrement de l'aéroport ( "+response.getName()+" ) échoué");
        }
        return statusResponse;
    }

    @Override
    public StatusResponse createAirportsFromTravelPayouts() {

        List<AirportTravelPayoutsDto> result = this.getAllAirportFromTravelPayouts().getContents();
        StatusResponse statusResponse = new StatusResponse();

        if(result.isEmpty()) {
            statusResponse.setCode(403);
            statusResponse.setMessage("La liste des aéroprt doit contenir au moins un élément");
        }

        for(AirportTravelPayoutsDto dto: result){
            this.create(dto);
        }

        statusResponse.setCode(200);
        statusResponse.setMessage("Les aéroports provenant de Travel Payouts ont été enrégistré avec succès");

        return statusResponse;
    }

    @Override
    public ResponseAirportRapidapi getByIataCodeFromTravelPayouts(String iataCode) {
        ResponseAirportRapidapi result = new ResponseAirportRapidapi();
        result.setContents(new ArrayList<>());
        result.setPage(0);

        AsyncHttpClient client = new DefaultAsyncHttpClient();
        CompletableFuture<Response> response =   client.prepare("GET", "https://airport-info.p.rapidapi.com/airport?iata="+iataCode)
                .setHeader("x-rapidapi-host", "airport-info.p.rapidapi.com")
                .setHeader("x-rapidapi-key", "e91d3d4939msha8db30c5f35418ep161835jsn47dccd980fa4")
                .execute()
                .toCompletableFuture();

        ObjectMapper mapper = new ObjectMapper();

                response.thenAccept(data -> {
                    try {
                        List<AirportRapidapiDto> res = new ArrayList<>();
                        AirportRapidapiDto airportRapidapiResult = mapper.readValue(data.getResponseBody(), mapper.getTypeFactory().constructType(AirportRapidapiDto.class));
                        res.add(airportRapidapiResult);
                        setDataAirportFromRapidapi(res);
                        result.setSize(res.size());
                        result.setContents(this.allResultRapidapi);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                })
                .join();

        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        result.setSize(this.allResultRapidapi.size());
        result.setContents(this.allResultRapidapi);



        return result;
    }

    @Override
    public ResponseTravelPayoutsdapi getByIata(String iataCode) {
        ResponseTravelPayoutsdapi result = new ResponseTravelPayoutsdapi();
        result.setPage(0);
        result.setContents(AirportTravelPayoutsDto.parse( this.airportRepository.findByCodeContainingIgnoreCase(iataCode)));
        result.setSize(result.getContents().size());
        return result;
    }

    @Override
    public ResponseAirportDistanceRapidapi getDistanceof2Airports(String iataAirport1, String iataAirport2) {
        ResponseAirportDistanceRapidapi result = new ResponseAirportDistanceRapidapi();
        AsyncHttpClient client = new DefaultAsyncHttpClient();
        CompletableFuture<Response> response =  client.prepare("GET", "https://aerodatabox.p.rapidapi.com/airports/iata/"+iataAirport1+"/distance-time/"+iataAirport2)
                .setHeader("x-rapidapi-host", "aerodatabox.p.rapidapi.com")
                .setHeader("x-rapidapi-key", "e91d3d4939msha8db30c5f35418ep161835jsn47dccd980fa4")
                .execute()
                .toCompletableFuture();

                ObjectMapper mapper = new ObjectMapper();
                response.thenAccept(
                        (data)->{
                            try {
                                List<AirportsDistanceToFromDto> list = new ArrayList<>();
                                AirportsDistanceToFromDto airportRapidapiResult = mapper.readValue(data.getResponseBody(), mapper.getTypeFactory().constructType( AirportsDistanceToFromDto.class));

                                list.add(airportRapidapiResult);
                                setDataAirportDistanceFromRapidapi(list);
                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                        }
                )
                .join();

        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        result.setContents(this.airportsDistanceToFromDto);
        result.setPage(0);
        result.setSize(result.getContents().size());

        return result;
    }

    public void setDataAirportFromRapidapi(List<AirportRapidapiDto> dtos){
        this.allResultRapidapi = dtos;
    }

    public void setDataAirportDistanceFromRapidapi(List<AirportsDistanceToFromDto> dtos){
        this.airportsDistanceToFromDto = dtos;
    }

    public void setDataAirportFromTravelPayouts(List<AirportTravelPayoutsDto> dtos){
        this.allResultTravelPayouts = dtos;
    }
}
