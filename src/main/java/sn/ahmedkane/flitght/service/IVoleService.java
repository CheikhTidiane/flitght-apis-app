package sn.ahmedkane.flitght.service;

import sn.ahmedkane.flitght.web.dto.SearchDto;
import sn.ahmedkane.flitght.web.dto.VolViewDto;

import java.util.List;

public interface IVoleService {

    public List<VolViewDto> getFlitghtByAirport(SearchDto searchDto);
}
