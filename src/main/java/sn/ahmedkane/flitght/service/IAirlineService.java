package sn.ahmedkane.flitght.service;

import sn.ahmedkane.flitght.persistance.entities.Airline;
import sn.ahmedkane.flitght.utils.StatusResponse;
import sn.ahmedkane.flitght.web.dto.AirlineRapidapiDto;
import sn.ahmedkane.flitght.web.dto.AirlineTravelPayoutsDto;
import sn.ahmedkane.flitght.web.dto.SearchAirlineDto;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirline;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirlineTravelPayouts;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirportRapidapi;

import java.util.List;

public interface IAirlineService {

    ResponseAirline getAirlineFromRapidapi();
    ResponseAirlineTravelPayouts getAirlineFromTravelPayouts();

    boolean saveAllFromRapidapiBody(List<AirlineRapidapiDto> airlineRapidapiDtoList);
    boolean saveAllFromTravelPayoutBody(List<AirlineTravelPayoutsDto> airlineTravelPayoutsDtos);

    StatusResponse saveAllFromRapidapi();

    StatusResponse saveAllFromTravelPayouts();

    ResponseAirline getAll(int page , int size);
    ResponseAirlineTravelPayouts getAllFromTravelPayouts(int page , int size);
    Airline getByExternalId(String externalId);
    List<AirlineRapidapiDto> searchByIata(String codeIata);
    List<AirlineRapidapiDto> searchByIcao(String codeIcao);
    List<AirlineRapidapiDto> searchByName(String name);
    ResponseAirline searchByParams(SearchAirlineDto dtoSearch, int page , int size);
}
