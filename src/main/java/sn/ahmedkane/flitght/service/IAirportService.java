package sn.ahmedkane.flitght.service;

import org.asynchttpclient.Response;
import org.springframework.web.bind.annotation.ResponseStatus;
import sn.ahmedkane.flitght.utils.StatusResponse;
import sn.ahmedkane.flitght.web.dto.AirportRapidapiDto;
import sn.ahmedkane.flitght.web.dto.AirportTravelPayoutsDto;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirportDistanceRapidapi;
import sn.ahmedkane.flitght.web.dto.response.ResponseAirportRapidapi;
import sn.ahmedkane.flitght.web.dto.response.ResponseTravelPayoutsdapi;

import java.net.http.HttpResponse;
import java.util.List;

public interface IAirportService {
     ResponseTravelPayoutsdapi getAllAirportFromTravelPayouts();
     ResponseAirportRapidapi getAllAirportFromRapidapi();

     ResponseTravelPayoutsdapi getAll(int page , int size);

     StatusResponse create(AirportTravelPayoutsDto dtoCreate);

     StatusResponse createAirportsFromTravelPayouts();

     ResponseAirportRapidapi getByIataCodeFromTravelPayouts(String iataCode);

     ResponseTravelPayoutsdapi getByIata(String iataCode);

     ResponseAirportDistanceRapidapi getDistanceof2Airports(String iataAirport1, String iataAirport2);


}
