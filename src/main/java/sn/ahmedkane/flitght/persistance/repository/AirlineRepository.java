package sn.ahmedkane.flitght.persistance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import sn.ahmedkane.flitght.persistance.entities.Airline;

import java.util.List;


@Repository
public interface AirlineRepository extends JpaRepository<Airline,Long>, QuerydslPredicateExecutor<Airline> {

    Airline findByLinkedMetaExternalId(String externalId);

    List<Airline> findByIataCodeContainingIgnoreCase(String iataCode);

    List<Airline> findByIcaoCodeContainingIgnoreCase(String icaoCode);

    List<Airline> findByNameContainingIgnoreCase(String name);

}
