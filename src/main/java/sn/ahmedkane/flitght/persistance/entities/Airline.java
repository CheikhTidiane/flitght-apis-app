package sn.ahmedkane.flitght.persistance.entities;

import javax.persistence.*;

@Entity
public class Airline {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "iata_code")
    private String iataCode;

    @Column(name = "cao_code")
    private String icaoCode;

    private String name;

    @JoinColumn(name = "linked_meta", referencedColumnName = "id", nullable = false)
    @OneToOne(optional = false, cascade = CascadeType.ALL)
    protected Meta linkedMeta;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    public String getIcaoCode() {
        return icaoCode;
    }

    public void setIcaoCode(String icaoCode) {
        this.icaoCode = icaoCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Meta getLinkedMeta() {
        return linkedMeta;
    }

    public void setLinkedMeta(Meta linkedMeta) {
        this.linkedMeta = linkedMeta;
    }
}
