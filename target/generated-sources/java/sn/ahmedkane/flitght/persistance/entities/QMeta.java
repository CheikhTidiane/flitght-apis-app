package sn.ahmedkane.flitght.persistance.entities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMeta is a Querydsl query type for Meta
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMeta extends EntityPathBase<Meta> {

    private static final long serialVersionUID = 1467557548L;

    public static final QMeta meta = new QMeta("meta");

    public final NumberPath<Integer> author = createNumber("author", Integer.class);

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath externalId = createString("externalId");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final DateTimePath<java.util.Date> lastModified = createDateTime("lastModified", java.util.Date.class);

    public final StringPath resourceType = createString("resourceType");

    public QMeta(String variable) {
        super(Meta.class, forVariable(variable));
    }

    public QMeta(Path<? extends Meta> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMeta(PathMetadata metadata) {
        super(Meta.class, metadata);
    }

}

